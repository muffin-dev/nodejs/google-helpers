# Muffin Dev for Node - Google Helpers

This module contains a bunch of tools and utilities to make the use of Google APIs easier.

## Supported APIs

- [*Google Spreadsheets*](https://developers.google.com/sheets/api)

## How to use?

```js
const path = require('path');
const GoogleAPIManager = require('..').default;

async function testReadData() {
  const manager = await GoogleAPIManager.create(
    path.join(__dirname, './credentials.json'),
    path.join(__dirname, './token.json'),
    'installed'
  );

  // This example reads a Google Sheet
  const data = await manager.spreadsheets.read('your-gsheet-id', 'Data!A1:C100');
  console.log('Loaded data', data);
}

testReadData();
```

### Get credentials

Using the Google APIs requires to use credentials, which can be provided from your *Google Cloud Project* by downloading the `credentials.json` file. You can also get that file by [enabling the *Google Sheets API* from the quickstart docs](https://developers.google.com/sheets/api/quickstart/nodejs).

### Generate a `token.json` file

You can use the `generateToken` command of this package to generate a new `token.json` file. First, update your `package.json`:

```json
"scripts": {
    "generateToken": "generateToken"
}
```

Then, run the command:

```bash
npm run generateToken -- <credentials-path> <credentials-type> <token-path> <... scopes>
```

- `<credentials-path>`: Path to your `credentials.json` file
- `<credentials-type>`: The credentials type to use from your `credentials.json` file (for example `web`, `installed`, ...) depending on your app's target device
- `<token-path>`: Path to your `token.json` file
- `<... scopes>`: The target scopes of the token to generate (e.g.: https://www.googleapis.com/auth/spreadsheets.readonly). You can add as many scopes as you want, separated by spaces