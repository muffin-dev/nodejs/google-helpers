#!/usr/bin/env node

import { TCredentialsType } from '../lib/auth';
import generateToken from './generate-token';

generateToken(process.argv[2], process.argv[3] as TCredentialsType, process.argv[4], ...process.argv.slice(5));
