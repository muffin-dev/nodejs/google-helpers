import readline from 'readline';
import { asAbsolute, writeFileAsync } from '@muffin-dev/node-helpers';
import { OAuth2Client } from 'googleapis-common';
import { Credentials } from 'google-auth-library';
import { createOAuth2Client, readCredentialsJSON, TCredentialsType } from '../lib/auth';

/**
 * Generates a token.json file, based on your credentials.json file and the given scopes.
 * @param credentialsFilePath The path to your credentials.json file, which you can download from Google APIs Console).
 * @param credentialsType The type of the credentials you want to use, depending on your target device. It must match with the credentials
 * types of you credentials.json.
 * @param tokenOutputFilePath The path to the token file you want to generate. This path should ends with "/token.json"
 * @param scopes The scopes that you want your new token to target.
 */
export async function generateToken(
  credentialsFilePath: string,
  credentialsType: TCredentialsType,
  tokenOutputFilePath: string,
  ...scopes: string[]
): Promise<void> {
  if (!credentialsFilePath) {
    console.error('Missing path to the "credentials.json" file. You can download it from your Google APIs Console.');
    return;
  }

  if (!credentialsType) {
    console.error('Missing credentials type. This second argument must match with the target device of your app, defined in the credentials.json file.');
    return;
  }

  if (!tokenOutputFilePath) {
    console.error('Missing path to the "token.json" file.');
    return;
  }

  const credentials = await readCredentialsJSON({ credentialsFilePath, credentialsType });
  const token = await getNewToken(createOAuth2Client(credentials), scopes);
  await generateTokenFile(tokenOutputFilePath, token);
}

/**
 * Asks the user to follow the authorization link to get a code in order to generate a new token.
 * @param oAuth2Client The authorization client instance to use.
 * @param scopes The scopes you want your new token to target.
 */
function getNewToken(oAuth2Client: OAuth2Client, scopes: string[]): Promise<Credentials> {
  return new Promise<Credentials>((resolve: (token: Credentials) => void, reject: (error: Error) => void) => {
    // Generate and display the authentication URL
    const authUrl = oAuth2Client.generateAuthUrl({ access_type: 'offline', scope: scopes });
    console.log('Please follow this link to authorize your app: ' + authUrl);

    // Create a Readline interface in order to get the authorization code
    const readlineInterface = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    readlineInterface.question('Follow the setps of the previous link, and enter the authorization code here: ', (code) => {
      readlineInterface.close();
      oAuth2Client.getToken(code, (err, token) => {
        if (err) {
          console.error('Error while trying to retrieve the access token', err);
          reject(err);
          return;
        }

        resolve(token as Credentials);
      });
    });
  });
}

/**
 * Generates a token file at the given path using the given credentials.
 * @async
 * @param tokenOutputFilePath The output path of the token file.
 * @param token The credentials data, which you can get using the getNewToken() method.
 */
async function generateTokenFile(tokenOutputFilePath: string, token: Credentials): Promise<void> {
  try {
    await writeFileAsync(asAbsolute(tokenOutputFilePath), JSON.stringify(token));
  }
  catch (error) {
    console.error('Error while generating the token.json file', error);
    return;
  }

  console.log('Token file generated at: ' + tokenOutputFilePath);
}

export default generateToken;
