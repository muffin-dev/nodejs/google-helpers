/**
 * @enum Indicates which dimension an operation should apply to.
 * Copied from https://developers.google.com/sheets/api/reference/rest/v4/Dimension
 */
export enum Dimension {
  /**
   * @property The default value, do not use.
   */
  DIMENSION_UNSPECIFIED = 'DIMENSION_UNSPECIFIED',

  /**
   * @property Operates on the rows of a sheet.
   */
  ROWS = 'ROWS',

  /**
   * @property Operates on the columns of a sheet.
   */
  COLUMNS = 'COLUMNS',
}

/**
 * @enum Determines how input data should be interpreted.
 * Copied from https://developers.google.com/sheets/api/reference/rest/v4/ValueInputOption
 */
export enum ValueInputOption {
  /**
   * @property Default input value. This value must not be used.
   */
  INPUT_VALUE_OPTION_UNSPECIFIED = 'INPUT_VALUE_OPTION_UNSPECIFIED',

  /**
   * @property Default input value. This value must not be used.
   */
  RAW = 'RAW',

  /**
   * @property The values will be parsed as if the user typed them into the UI. Numbers will stay as numbers, but strings may be converted
   * to numbers, dates, etc. following the same rules that are applied when entering text into a cell via the Google Sheets UI.
   */
  USER_ENTERED = 'USER_ENTERED',
}

// eslint-disable-next-line no-use-before-define
export type Value = null | string | number | boolean | Map<string, Value> | ListValue;

/**
 * @interface ListValue ListValue is a wrapper around a repeated field of values.
 * Copied from https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.ListValue
 */
export interface ListValue {
  /**
   * @property Repeated field of dynamically typed values.
   */
  values: Value;
}

/**
 * @interface ValueRange Data within a range of the spreadsheet.
 * Copied from https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values#ValueRange
 */
export interface ValueRange {
  range: string;
  majorDimension: Dimension;
  values: ListValue[];
}
