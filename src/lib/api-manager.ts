import { OAuth2Client } from 'googleapis-common';
import { Credentials } from 'google-auth-library';
import { GoogleAPIHelperBase } from './api-helper-base';
import { authorize, ICredentialsData, readCredentialsJSON, readTokenJSON, TCredentialsType } from './auth';
import { GoogleSpreadsheets } from './spreadsheets';

/**
 * @interface IGoogleAPIHelperInfos Represents informations about a loaded API Helper.
 */
interface IGoogleAPIHelperInfos {
  name: string;
  oAuth2Client?: OAuth2Client;
  instance: GoogleAPIHelperBase;
}

const API_HELPERS = {
  spreadsheets: 'spreadsheets'
};

export class GoogleAPIManager {

  //#region Properties

  /**
   * @property Contains the default authorization client of the loaded API helpers.
   */
  private _oAuth2Client: OAuth2Client = null;

  /**
   * @property Contains all the loaded API helpers.
   */
  private _apiHelpers = new Map<string, IGoogleAPIHelperInfos>();

  //#endregion


  //#region Initialization

  /**
   * Class constructor.
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() { }

  /**
   * Creates a new GoogleAPIManager instance, which will help you to use the different APIs with only one entry point.
   * @param credentials The loaded credentialsyou want to use (from credentials.json file, which you can download from Google API Console).
   * @param token The loaded token (from token.json file, which you can generate using the generateToken command).
   */
  public static async create(credentials: ICredentialsData, token: Credentials): Promise<GoogleAPIManager>;

  /**
   * Creates a new GoogleAPIManager instance, which will help you to use the different APIs with only one entry point.
   * @param credentialsFilePath The path to the credentials.json file you want to use (which you can download from Google APIs Console).
   * @param tokenFilePath The path to your token.json file (which you can generate using the generateToken command).
   * @param credentialsType Defines the types of credentials you want to use from the credentials.json file (depending on your app's target
   * device).
   */
  public static async create(
    credentialsFilePath: string,
    tokenFilePath: string,
    credentialsType: TCredentialsType
  ): Promise<GoogleAPIManager>;

  /**
   * Creates a new GoogleAPIManager instance, which will help you to use the different APIs with only one entry point.
   * @param credentials The loaded credentials or path to the credentials.json file (which you can download from Google APIs Console).
   * @param token The loaded token or path to your token.json file (which you can generate using the generateToken command).
   * @param credentialsType Defines the types of credentials you want to use from the credentials.json file (used only if you passed a file
   * path as the value of the credentials parameter).
   */
  public static async create(
    credentials: string | ICredentialsData,
    token: string | Credentials,
    credentialsType: TCredentialsType = null
  ): Promise<GoogleAPIManager> {
    const manager = new GoogleAPIManager();
    if (!credentials) {
      throw new Error('Missing credentials data or file path');
    }
    if (!credentials) {
      throw new Error('Missing token data or file path');
    }

    if (typeof credentials === 'string') {
      credentials = await readCredentialsJSON({ credentialsFilePath: credentials, credentialsType });
    }
    if (typeof token === 'string') {
      token = await readTokenJSON(token);
    }

    manager._oAuth2Client = authorize(credentials, token);
    return manager;
  }

  //#endregion


  //#region API helpers

  /**
   * Gets the loaded API helper for Google Spreadsheets if it exists, or create it.
   */
  public get spreadsheets(): GoogleSpreadsheets {
    return this._apiHelperGetter(API_HELPERS.spreadsheets, GoogleSpreadsheets);
  }

  /**
   * Creates an instance of GoogleSpreadsheets API helper, or reset the existing one, using the given credentials.
   * @param credentials The loaded credentialsyou want to use (from credentials.json file, which you can download from Google API Console).
   * @param token The loaded token (from token.json file, which you can generate using the generateToken command).
   * @returns Returns the created API helper instance.
   */
  public initSpreadsheets(credentials: ICredentialsData, token: Credentials): Promise<GoogleSpreadsheets>;

  /**
   * Creates an instance of GoogleSpreadsheets API helper, or reset the existing one, using the given credentials.
   * @param credentialsFilePath The path to the credentials.json file you want to use (which you can download from Google APIs Console).
   * @param tokenFilePath The path to your token.json file (which you can generate using the generateToken command).
   * @param credentialsType Defines the types of credentials you want to use from the credentials.json file (depending on your app's target
   * device).
   * @returns Returns the created API helper instance.
   */
  public initSpreadsheets(
    credentialsFilePath: string,
    tokenFilePath: string,
    credentialsType: TCredentialsType
  ): Promise<GoogleSpreadsheets>;

  /**
   * Creates an instance of GoogleSpreadsheets API helper, or reset the existing one, using the given credentials.
   * @param credentials The loaded credentials or path to the credentials.json file (which you can download from Google APIs Console).
   * @param token The loaded token or path to your token.json file (which you can generate using the generateToken command).
   * @param credentialsType Defines the types of credentials you want to use from the credentials.json file (used only if you passed a file
   * path as the value of the credentials parameter).
   * @returns Returns the created API helper instance.
   */
  public initSpreadsheets(
    credentials: string | ICredentialsData,
    token: string | Credentials,
    credentialsType: TCredentialsType = null
  ): Promise<GoogleSpreadsheets> {
    return this._initHelper(API_HELPERS.spreadsheets, GoogleSpreadsheets, credentials, token, credentialsType || null);
  }

  /**
   * Creates an instance of GoogleSpreadsheets API helper, or reset the existing one, using the credentials files at the given path.
   * @param filePaths The path to your credentials files.
   */
  // public initSpreadsheets(filePaths: ICredentialsFilesOptions): Promise<GoogleSpreadsheets> {
  //   return this._initHelper(API_HELPERS.spreadsheets, filePaths, GoogleSpreadsheets);
  // }

  //#endregion


  //#region Private methods

  /**
   * Gets the loaded API helper of the given name, or initializes a new instance of the expected API helper type.
   * @param name The name of the API helper (used to find it in the loaded helpers map).
   * @param ctor The constructor of the API helper you want to register.
   * @returns Returns the loaded helper, or its brand new instance.
   */
  private _apiHelperGetter<T extends GoogleAPIHelperBase>(name: string, ctor: new (authClient: OAuth2Client) => T) {
    if (this._apiHelpers.has(name)) {
      return this._apiHelpers.get(name).instance as T;
    }

    // eslint-disable-next-line new-cap
    const spreadsheetshelperInstance = new ctor(this._oAuth2Client);
    this._apiHelpers.set(name, {
      name: name,
      instance: spreadsheetshelperInstance
    });
    return spreadsheetshelperInstance;
  }

  /**
   * Creates an instance of an API helper, or reset the existing one, using the credentials files at the given path.
   * @param name The name of the API helper (used to find it in the loaded helpers map).
   * @param ctor The constructor of the API helper you want to register.
   * @param credentials The loaded credentials or path to the credentials.json file (which you can download from Google APIs Console).
   * @param token The loaded token or path to your token.json file (which you can generate using the generateToken command).
   * @param credentialsType Defines the types of credentials you want to use from the credentials.json file (used only if you passed a file
   * path as the value of the credentials parameter).
   * @returns Returns the created API helper instance.
   */
  private async _initHelper<T extends GoogleAPIHelperBase>(
    name: string,
    ctor: new (authClient: OAuth2Client) => T,
    credentials: string | ICredentialsData,
    token: string | Credentials,
    credentialsType: TCredentialsType = null
  ): Promise<T> {
    if (!credentials) {
      throw new Error('Missing credentials data or file path');
    }
    if (!credentials) {
      throw new Error('Missing token data or file path');
    }

    if (typeof credentials === 'string') {
      credentials = await readCredentialsJSON({ credentialsFilePath: credentials, credentialsType });
    }
    if (typeof token === 'string') {
      token = await readTokenJSON(token);
    }

    const authClient = authorize(credentials, token);
    // eslint-disable-next-line new-cap
    const instance = new ctor(authClient);
    this._apiHelpers.set(name, {
      name,
      instance,
      oAuth2Client: authClient
    });
    return instance;
  }

  //#endregion

}
