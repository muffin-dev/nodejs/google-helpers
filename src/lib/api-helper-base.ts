import { OAuth2Client } from 'googleapis-common';

export abstract class GoogleAPIHelperBase {

  /**
   * @property The authorization client to use for this API helper.
   */
  private _oAuth2Client: OAuth2Client = null;

  public getOAuth2Client(): OAuth2Client {
    return this._oAuth2Client;
  }

  /**
   * Class constructor.
   * @param oAuth2Client The authorization client to use for this API helper.
   */
  constructor(oAuth2Client: OAuth2Client) {
    this._oAuth2Client = oAuth2Client;
  }

}
