import { asAbsolute, readJSONFileAsync } from '@muffin-dev/node-helpers';
import { google } from 'googleapis';
import { OAuth2Client } from 'googleapis-common';
import { Credentials } from 'google-auth-library';
import { ICredentialsData, ICredentialsFilesOptions } from './interfaces';

/**
 * Uses the given credentials to generate an OAuth2Client instance.
 * @param credentialsData The credentials of your app. Use the readCredentialsJson() to get the content of your credentials.json file,
 * which you can download from Google APIs Console.
 * @param authToken The authentication token. Use the readTokenJSON() to get the content of your token.json file, which you can generate
 * using the generateToken command of this module.
 */
export function authorize(credentialsData: ICredentialsData, authToken: Credentials): OAuth2Client {
  const authClient = createOAuth2Client(credentialsData);
  authClient.setCredentials(authToken);
  return authClient;
}

/**
 * Uses the given credential files to generate an OAuth2Client instance.
 * @param filePaths The paths of the credentials files to use.
 */
export async function authorizeFromFiles(filePaths: ICredentialsFilesOptions): Promise<OAuth2Client> {
  const credentials = await readCredentialsJSON({ ...filePaths });
  const token = await readTokenJSON(filePaths.tokenFilePath);
  return authorize(credentials, token);
}

/**
 * Creates an OAuth2Client instance using the given credentials data.
 * @param credentialsData The data loaded from a credentials.json file, which you can read by usingthe readCredentialsJSON() method.
 * @returns Returns the created OAuth2Client instance.
 */
export function createOAuth2Client(credentialsData: ICredentialsData): OAuth2Client {
  return new google.auth.OAuth2(
    credentialsData.client_id,
    credentialsData.client_secret,
    credentialsData.redirect_uris[0]
  );
}

/**
 * Extracts the content of a credentials.json file.
 * @param credentialsOptions The path to your credentials.json file, and the eventual credential type to use.
 */
export async function readCredentialsJSON(credentialsOptions: Pick<ICredentialsFilesOptions, 'credentialsFilePath' | 'credentialsType'>): Promise<ICredentialsData> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let credentials = await readJSONFileAsync(asAbsolute(credentialsOptions.credentialsFilePath)) as any;
  if (credentialsOptions.credentialsType) {
    credentials = credentials[credentialsOptions.credentialsType];
  }
  return credentials as ICredentialsData;
}

/**
 * Extracts the content of a token.json file. You can use the generateToken command of this module to generate one.
 * @param tokenFilePath The path to your token.json file.
 */
export async function readTokenJSON(tokenFilePath: string): Promise<Credentials> {
  return await readJSONFileAsync(asAbsolute(tokenFilePath)) as Credentials;
}
