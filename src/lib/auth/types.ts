/**
 * @type Defines the types of credentials you're using from a credentials.json file.
 */
export type TCredentialsType = 'installed' | 'web' | null;
