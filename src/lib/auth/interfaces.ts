/* eslint-disable camelcase */

import { TCredentialsType } from './types';

/**
 * @interface ICredentialsData Represents the content of a credentials.json file (which you can download from Google APIs Console).
 */
export interface ICredentialsData {
  client_id: string;
  project_id: string;
  auth_uri: string;
  token_uri: string;
  auth_provider_x509_cert_url: string;
  client_secret: string;
  redirect_uris: string[];
}

/**
 * @interface ICredentialsFilesOptions Represents informations about the credentials files to use to authenticate an API user.
 */
export interface ICredentialsFilesOptions {
  /**
   * @property The path to the credentials.json file you want to read (you can download it from Google APIs Console).
   */
  credentialsFilePath: string;

  /**
   * @property The path to your token.json file.
   */
  tokenFilePath: string;

  /**
   * @property Defines the types of credentials you want to use from the credentials.json file.
   */
  credentialsType?: TCredentialsType;
}
