import { google } from 'googleapis';
import { GoogleAPIHelperBase } from '../api-helper-base';
import { Dimension, ValueInputOption, ValueRange } from '../interfaces';

export class GoogleSpreadsheets extends GoogleAPIHelperBase {

  /**
   * Reads a given rnge of data in the spreadsheet with the given id.
   * @param spreadsheetId The id of the spritesheet you want to read.
   * @param range The range of the data you want to get, with format "Datasheet!A1:H100".
   */
  public read(spreadsheetId: string, range: string): Promise<unknown[][]> {
    return new Promise<unknown[][]>((resolve: (data: unknown[][]) => void, reject: (err: Error) => void) => {
      const sheets = google.sheets({ version: 'v4', auth: this.getOAuth2Client() });
      sheets.spreadsheets.values.get(
        { spreadsheetId, range },
        (err, res) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(res.data.values);
        }
      );
    });
  }

  /**
   * Writes the given values in a spreadsheet.
   * @param spreadsheetId The id of the spritesheet you want to read.
   * @param range The range of data you want to update, with format "Datasheet!A1:H100".
   * @param values An instance of ValueRange that represents the informations to set. See
   * https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values#ValueRange for more informations.
   * @param valueInputOption Defines the way the given values are parsed when written in the spreadsheet.
   */
  public write(
    spreadsheetId: string,
    range: string,
    values: ValueRange,
    valueInputOption: ValueInputOption = ValueInputOption.USER_ENTERED
  ): Promise<void> {
    if (!valueInputOption) { valueInputOption = ValueInputOption.USER_ENTERED; }

    return new Promise((resolve: () => void, reject: (err: Error) => void) => {
      const sheets = google.sheets({ version: 'v4', auth: this.getOAuth2Client() });
      sheets.spreadsheets.values.update({
        spreadsheetId,
        range,
        valueInputOption,
        requestBody: {
          range: values.range,
          majorDimension: values.majorDimension,
          values: values.values as unknown as unknown[][]
        }
      }, (err) => {
        if (err) {
          reject(err);
          return;
        }
        resolve();
      });
    });
  }

  /**
   * Writes the content of the given cell in the spreadsheet.
   * @param spreadsheetId The id of the spritesheet you want to read.
   * @param sheetName The name of the sheet you want to update.
   * @param coords The coordinates of the cell to update on the sheet (with format "E123")
   * @param value The new value of the cell to update.
   */
  public writeCell(spreadsheetId: string, sheetName: string, coords: string, value: string): Promise<void> {
    return new Promise((resolve: () => void, reject: (err: Error) => void) => {
      const sheets = google.sheets({ version: 'v4', auth: this.getOAuth2Client() });
      const range = `${sheetName}!${coords}:${coords}`;
      sheets.spreadsheets.values.update({
        spreadsheetId,
        range: range,
        valueInputOption: ValueInputOption.USER_ENTERED,
        requestBody: {
          range: range,
          majorDimension: Dimension.ROWS,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          values: [ [ value ] ] as any[]
        }
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      }, (err: any) => {
        if (err) {
          reject(err);
          return;
        }
        resolve();
      });
    });
  }

  /**
   * Writes the given values in a spreadsheet.
   * @param spreadsheetId The id of the spritesheet you want to read.
   * @param sheetName The name of the sheet you want to update.
   * @param row The number of the row you want to update. NOTE: In a spreadsheet, the first row number is 1, not 0.
   * @param fromCol The letter of the first column of the values you want to update on the defined row.
   * @param toCol The letter of the last column of the values you want to update on the defined row.
   * @param values An array of string values used to replace the values on the row.
   */
  public writeRow(spreadsheetId: string, sheetName: string, row: number, fromCol: string, toCol: string, values: string[]): Promise<void> {
    return new Promise((resolve: () => void, reject: (err: Error) => void) => {
      const sheets = google.sheets({ version: 'v4', auth: this.getOAuth2Client() });
      const range = `${sheetName}!${fromCol}${row}:${toCol}${row}`;
      sheets.spreadsheets.values.update({
        spreadsheetId,
        range: range,
        valueInputOption: ValueInputOption.USER_ENTERED,
        requestBody: {
          range: range,
          majorDimension: Dimension.ROWS,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          values: [ values ] as any[]
        }
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      }, (err: any) => {
        if (err) {
          reject(err);
          return;
        }
        resolve();
      });
    });
  }

}
